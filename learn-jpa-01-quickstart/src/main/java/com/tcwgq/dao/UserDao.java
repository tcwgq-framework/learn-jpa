package com.tcwgq.dao;

import com.tcwgq.bean.User;

import java.util.List;

public interface UserDao {
	public void save(User user);

	public void update(User user);

	public void delete(Integer id);

	public User getUser(Integer id);

	public List<User> getAllUser();
}
