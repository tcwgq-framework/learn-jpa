package com.tcwgq.bean;

import javax.persistence.*;
import java.beans.Transient;
import java.util.Arrays;
import java.util.Date;

@Entity
@Table(name = "t_user")
public class User {
	private Integer id;
	private String name;
	private Date birthday;
	// 设置默认值
	private Gender gender = Gender.male;
	private String text;
	private byte[] file;
	private String path;

	@Transient
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Lob
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Lob
	// 延迟加载
	@Basic(fetch = FetchType.LAZY)
	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

	public User() {
		super();
	}

	public User(Integer id) {
		super();
		this.id = id;
	}

	public User(String name) {
		super();
		this.name = name;
	}

	public User(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	@Id
	// 默认就是GenerationType.AUTO，可以省略后面的配置
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name", length = 50, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Temporal(TemporalType.DATE)
	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Enumerated(EnumType.STRING)
	@Column(length = 6, nullable = false)
	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", birthday=" + birthday
				+ ", gender=" + gender + ", text=" + text + ", file="
				+ Arrays.toString(file) + ", path=" + path + "]";
	}

}
