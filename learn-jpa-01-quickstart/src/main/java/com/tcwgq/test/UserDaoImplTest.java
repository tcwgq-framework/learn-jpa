package com.tcwgq.test;

import com.tcwgq.bean.User;
import com.tcwgq.dao.UserDao;
import com.tcwgq.dao.impl.UserDaoImpl;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class UserDaoImplTest {
	private UserDao userDao = new UserDaoImpl();

	@Test
	public void testSave() {
		for (int i = 1; i <= 10; i++) {
			User user = new User("name_" + i);
			userDao.save(user);
		}
	}

	@Test
	public void testUpdate() {
		User user = new User(1, "zhangSan");
		userDao.update(user);
	}

	@Test
	public void testDelete() {
		userDao.delete(1);
	}

	@Test
	public void testGetUser() {
		User user = userDao.getUser(2);
		System.out.println(user);
	}

	@Test
	public void testGetUserByReference() {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("simple");
		EntityManager em = factory.createEntityManager();
		User user = em.getReference(User.class, 15);
		System.out.println(user);
		em.close();
		factory.close();
	}

	@Test
	public void testUpdate1() {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("simple");
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		User user = em.find(User.class, 1);
		user.setName("zhangSan");
		em.getTransaction().commit();
		em.close();
		factory.close();
	}

	@Test
	public void testUpdate2() {
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("simple");
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		User user = em.find(User.class, 1);
		em.clear();
		user.setName("zhangSan");
		em.getTransaction().commit();
		em.close();
		factory.close();
	}

	@Test
	public void testGetAllUser() {
		List<User> list = userDao.getAllUser();
		for (User user : list) {
			System.out.println(user);
		}
	}

}
