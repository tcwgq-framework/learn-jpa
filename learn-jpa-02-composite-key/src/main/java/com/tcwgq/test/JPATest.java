package com.tcwgq.test;

import com.tcwgq.bean.AirLine;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPATest {
	private EntityManagerFactory factory = Persistence
			.createEntityManagerFactory("simple");

	@Test
	public void save() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		AirLine airline = new AirLine("PEK", "SHA", "北京-上海");
		em.persist(airline);
		em.getTransaction().commit();
		em.close();
		factory.close();
	}

}
