package com.tcwgq.bean;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "t_airline")
public class AirLine {
	private AirLinePK id;
	private String name;

	public AirLine() {
		super();
	}

	public AirLine(String start, String end, String name) {
		this.id = new AirLinePK(start, end);
		this.name = name;
	}

	@EmbeddedId
	public AirLinePK getId() {
		return id;
	}

	public void setId(AirLinePK id) {
		this.id = id;
	}

	@Column(length = 50)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
