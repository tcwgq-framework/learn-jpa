package com.tcwgq.bean.joined;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "ft_emp")
@PrimaryKeyJoinColumn(name = "ft_id")
public class FullTimeEmployee extends Employee {
	private Double salary;

	@Column
	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

}
