package com.tcwgq.bean.table_per_class;

import javax.persistence.*;

@Entity
@Table(name = "emp")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Employee {
	private Integer id;
	private String name;

	public Employee() {
		super();
	}

	public Employee(Integer id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(length = 50, nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
