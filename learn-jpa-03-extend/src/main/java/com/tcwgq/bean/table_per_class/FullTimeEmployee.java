package com.tcwgq.bean.table_per_class;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ft_emp")
public class FullTimeEmployee extends Employee {
	private Double salary;

	@Column
	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

}
