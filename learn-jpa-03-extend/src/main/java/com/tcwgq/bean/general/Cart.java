package com.tcwgq.bean.general;

public class Cart {
	protected Integer operationCount; // transient state

	public Cart() {
		operationCount = 0;
	}

	public Integer getOperationCount() {
		return operationCount;
	}

	public void incrementOperationCount() {
		operationCount++;
	}

}
