package com.tcwgq.test;

import com.tcwgq.bean.IDCard;
import com.tcwgq.bean.Person;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPATest {
	private EntityManagerFactory factory = Persistence
			.createEntityManagerFactory("simple");

	@Test
	public void save() {
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		Person person = new Person();
		person.setName("张三");
		IDCard card = new IDCard();
		card.setCardno("123456789");
		person.setIdcard(card);
		card.setPerson(person);
		em.persist(person);
		em.getTransaction().commit();
		em.close();
		factory.close();
	}
}
